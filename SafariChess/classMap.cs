﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SafariChess
{
    class classMap : ICloneable<classMap>
    {
        public string tipe;
        public Image image;
        public int indexAnimal;

        public classMap Clone()
        {
            return new classMap(tipe,indexAnimal) { };
        }
        public classMap(string tipe, int indexAnimal)
        {
            this.indexAnimal = indexAnimal;
            this.tipe = tipe;
        }

        public classMap (int i) {
            if (i == 2 || i == 4 || i == 10)
            {
                tipe = "shield-p2";
                image = Image.FromFile("board/shield.JPG");
            }
            else if (i == 52 || i == 58 || i == 60)
            {
                tipe = "shield-p1";
                image = Image.FromFile("board/shield.JPG");
            }
            else if (i == 3)
            {
                tipe = "star-p2";
                image = Image.FromFile("board/core.JPG");
            }
            else if (i == 59)
            {
                tipe = "star-p1";
                image = Image.FromFile("board/core.JPG");
            }
            else if (i == 22 || i == 23 || i == 25 || i == 26 || i == 29 || i == 30 || i == 32 || i == 33 || i == 36 || i == 37 || i == 39 || i == 40)
            {
                tipe = "water";
                image = Image.FromFile("board/tile_water.JPG");
            }
            else
            {
                tipe = "ground";
                image = Image.FromFile("board/tile_ground.JPG");
            }
            image = new Bitmap(image, new Size(50, 50));
            this.indexAnimal = -1;
        }
    }
}
