﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SafariChess
{
    public partial class Form1 : Form
    {
        string LAWAN_AI = "p2";
        string turn = "p1";
        int ctrTurn = 0; int indexButtonSelected = -1; //untuk update gambar pada button
        int indexAnimalMove = -1; //untuk update pada classMap
        classAnimal animalTurn;

        List<Button> listButton = new List<Button>();
        List<classMap> listMap = new List<classMap>();
        List<classAnimal> listP1 = new List<classAnimal>();
        List<classAnimal> listP2 = new List<classAnimal>();

        int depth = 3;
        int ctrMulai = 0; //untuk menjaga pos
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Size = new Size(600, 500);
            this.Location = new Point(500, 200);
            createButton();
            createListAnimal();
            startMiniMax();
        }

        public void createButton() {
            for (int i = 0; i < 63; i++)
            {
                Button b = new Button();
                b.Location = new Point(i % 7 * 49 + 100, i / 7 * 49);
                b.Size = new Size(50, 50);
                b.Name = "btn" + i;
                listMap.Add(new classMap(i));

                Image image = listMap[listMap.Count-1].image;
                b.Image = image;
                listButton.Add(b);
                b.MouseClick += new MouseEventHandler(btnAdd_Click);
                this.Controls.Add(b);
            }
        }
        public void createListAnimal() {
            for (int i = 0; i < 2; i++)
            {
                string player = "";
                if (i == 0) player = "p1";
                else player = "p2";
                for (int j = 0; j < 8; j++)
                {
                    if (i == 0)
                    {
                        listP1.Add(new classAnimal(j, player));
                        Image image = listP1[listP1.Count - 1].gambar;
                        listButton[listP1[listP1.Count - 1].posisi].Image = image;
                        listMap[listP1[listP1.Count - 1].posisi].indexAnimal = listP1.Count - 1;
                    }
                    else
                    {
                        listP2.Add(new classAnimal(j, player));
                        Image image = listP2[listP2.Count - 1].gambar;
                        listButton[listP2[listP2.Count - 1].posisi].Image = image;
                        listMap[listP2[listP2.Count - 1].posisi].indexAnimal = listP2.Count - 1;
                    }
                }
            }
        }

        private void btnAdd_Click(object sender, MouseEventArgs e)
        {
            Button b = (Button)sender;
            int idBtn = Convert.ToInt32(b.Name.Substring(3));
            bool cekGerak = true;

            List<classAnimal> listPlayerGerak;
            if (turn == "p2") listPlayerGerak = listP2;
            else listPlayerGerak = listP1;

            for (int i = 0; i < listPlayerGerak.Count; i++)
            {
                if (listPlayerGerak[i].posisi == idBtn && listPlayerGerak[i].status == "alive")
                {
                    enableAllButton();
                    indexButtonSelected = idBtn;
                    ctrTurn = 1;
                    animalTurn = listPlayerGerak[i];
                    b.Enabled = false;
                    cekGerak = false;
                    indexAnimalMove = i;
                }
            }
            if (ctrTurn == 1) {
                if (cekGerak)
                {
                    if (animalTurn.possibleMove(idBtn, listMap, listP1, listP2, indexAnimalMove))
                    {
                        //MessageBox.Show("valid");
                        moveAnimal(idBtn, animalTurn.posisi, indexAnimalMove);
                        listButton[indexButtonSelected].Image = listMap[indexButtonSelected].image;
                        listButton[idBtn].Image = animalTurn.gambar;
                        enableAllButton();
                        ctrTurn = 0;
                        if (turn == "p2") turn = "p1";
                        else turn = "p2";

                        // ------------------ MULAI AI ------------------------ //
                        if (cekWin()) {
                            startMiniMax();
                            cekWin();
                        }
                    }
                    //else MessageBox.Show("Gk valid!!!");
                }
            }
        }
        public void enableAllButton() {
            for (int i = 0; i < listButton.Count; i++)
            {
                listButton[i].Enabled = true;
            }
        }
        public Boolean cekWin() {
            if (listMap[3].indexAnimal != -1 && listP1[listMap[3].indexAnimal].posisi == 3)
            {
                MessageBox.Show("Player 1 WIN!!!");
                return false;
            }
            else if (listMap[59].indexAnimal != -1 && listP2[listMap[59].indexAnimal].posisi == 59) {
                MessageBox.Show("Player 2 WIN!!!");
            }
            return true;
        }
        public void moveAnimal(int tujuan, int posisi, int indexAnimalTurn) {
            List<classAnimal> listAnimal = new List<classAnimal>();
            List<classAnimal> listAnimalAlly = new List<classAnimal>();
            if (turn == "p1")
            {
                listAnimal = listP2;
                listAnimalAlly = listP1;
            }
            else if (turn == "p2")
            {
                listAnimal = listP1;
                listAnimalAlly = listP2;
            }
            listAnimalAlly[indexAnimalTurn].moveAnimal(listMap, listAnimal, tujuan, indexAnimalTurn);
            //if (listMap[tujuan].indexAnimal != -1)
            //{
            //    if (listAnimal[listMap[tujuan].indexAnimal].posisi == tujuan && listAnimal[listMap[tujuan].indexAnimal].power <= listAnimalAlly[listMap[posisi].indexAnimal].power)
            //    {
            //        listAnimal[listMap[tujuan].indexAnimal].status = "dead";
            //    }
            //}
            //listMap[posisi].indexAnimal = -1;
            //listMap[tujuan].indexAnimal = indexAnimalTurn;
            //listAnimalAlly[indexAnimalTurn].posisi = tujuan;
        }


        //------------------------------FUNCTION AI--------------------------------------//
        public void moveAnimalAI(int animal, int move) {
            if (turn == "p1") listAnimalAI = listP1;
            else if (turn == "p2") listAnimalAI = listP2;
            int posisiAnimal = listAnimalAI[animal].posisi;
            int tujuanAnimal = posisiAnimal;

            if (move == 0){//gerak i++
                tujuanAnimal += gerakAI(0, animal, posisiAnimal);
            }
            else if (move == 1){//gerak i--
                tujuanAnimal += gerakAI(1, animal, posisiAnimal);
            }
            else if (move == 2){//gerak j++
                tujuanAnimal += gerakAI(2, animal, posisiAnimal);
            }
            else if (move == 3){//gerak j--
                tujuanAnimal += gerakAI(3, animal, posisiAnimal);
            }
            
            listButton[posisiAnimal].Image = listMap[posisiAnimal].image;
            listButton[tujuanAnimal].Image = listAnimalAI[animal].gambar;
            moveAnimal(tujuanAnimal, posisiAnimal, animal);
            if (turn == "p2") turn = "p1";
            else turn = "p2";
        }

        int score;
        List<classAnimal> listAnimalAI = new List<classAnimal>();
        List<classAnimal> listAnimalAIOpp = new List<classAnimal>();
        List<classMap> listMapAI = new List<classMap>();
        public void startMiniMax() {
            if (turn == "p1") listAnimalAI = listP1;
            else listAnimalAI = listP2;

            int indexJ = -1, indexI = -1;
            int maxSBE = -99999;
            for (int i = 0; i < listAnimalAI.Count; i++)
            {
                //MessageBox.Show("tes");
                for (int j = 0; j < 4; j++)
                {
                    cloningGerakAwal();
                    int posisiAnimal = listAnimalAI[i].posisi;
                    int tujuanAnimal = listAnimalAI[i].posisi;
                    if (j == 0) tujuanAnimal += gerakAI(0, i, posisiAnimal);
                    else if (j == 1) tujuanAnimal += gerakAI(1, i, posisiAnimal);
                    else if (j == 2) tujuanAnimal += gerakAI(2, i, posisiAnimal); 
                    else if (j == 3) tujuanAnimal += gerakAI(3, i, posisiAnimal);

                    if (tujuanAnimal > 0 && tujuanAnimal < 63) {// supaya g offset index arraylist possiblemove
                        if (listAnimalAI[i].status == "alive" && listAnimalAI[i].possibleMove(tujuanAnimal, listMap, listP1, listP2, i))
                        {
                            //moveAnimalAIRecursive(tujuanAnimal, posisiAnimal, i);
                            //MessageBox.Show("Sebelum : " + listAnimalAI[i].posisi + "");
                            listAnimalAI[i].moveAnimal(listMapAI, listAnimalAIOpp, tujuanAnimal, i);
                            //MessageBox.Show("Sesudah : " + listAnimalAI[i].posisi + "");
                            int hasilSBE = -negaMax(depth,int.MaxValue*-1,int.MaxValue, turn, listAnimalAIOpp, listAnimalAI, listMapAI);
                            
                            //pengecekan win
                            if (SBEgotoShield(listMapAI, listAnimalAI, listAnimalAIOpp) != 0) hasilSBE = SBEgotoShield(listMapAI, listAnimalAI, listAnimalAIOpp);
                            if (SBEgotoWin(listMapAI, listAnimalAI, listAnimalAIOpp) != 0) hasilSBE = SBEgotoWin(listMapAI, listAnimalAI, listAnimalAIOpp);
                            //if (i == 0 && j == 2) MessageBox.Show("tikus" + hasilSBE);
                            //if (i == 2 && j == 1) MessageBox.Show("anjing" + hasilSBE);
                            //if (i == 6 && j == 2) MessageBox.Show("harimau" +hasilSBE);
                            //MessageBox.Show("Hasil SBE ke " + i + " : " + hasilSBE);
                            if (hasilSBE >= maxSBE)
                            {
                                maxSBE = hasilSBE;
                                //MessageBox.Show(maxSBE + "");
                                indexI = i;
                                indexJ = j;
                            }
                        }
                    }
                }
            }
            //moveAnimalAI(indexI, indexJ);

            //jaga base
            if (ctrMulai < 5)
            {
                ctrMulai++;
                if (turn == "p1" && ctrMulai % 2 == 1)
                {
                    if (ctrMulai == 1) { indexI = 3; indexJ = 0; }
                    else if (ctrMulai == 3) { indexI = 4; indexJ = 0; }
                    else if (ctrMulai == 5) { indexI = 2; indexJ = 0; }
                }
                else if (turn == "p2" && ctrMulai % 2 == 1)
                {
                    if (ctrMulai == 1) { indexI = 3; indexJ = 1; }
                    else if (ctrMulai == 3) { indexI = 4; indexJ = 1; }
                    else if (ctrMulai == 5) { indexI = 2; indexJ = 1; }
                }
                moveAnimalAI(indexI, indexJ);
            }
            else
            {
                //gerak sesuai NEGAMAX
                moveAnimalAI(indexI, indexJ);
            }
        }

        private int negaMax(int depth,int alpha,int beta, string player, List<classAnimal>listAlly, List<classAnimal>listOpponent, List<classMap>listMapAI)
        {
            if (depth == 0) return evaluate(listMapAI, listAlly, listOpponent);
            int max = -99999;

            if (player == "p1") player = "p2";
            else player = "p1";
            
            for (int i = 0; i < listAnimalAI.Count; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    List<classAnimal> listAllytemp = listAlly.ConvertAll(classAnimal => classAnimal.Clone());
                    List<classAnimal> listOpponenttemp = listOpponent.ConvertAll(classAnimal => classAnimal.Clone());
                    List<classMap> listMapAItemp = listMapAI.ConvertAll(classMap => classMap.Clone());

                    //gerak
                    int posisiAnimal = listAllytemp[i].posisi;
                    int tujuanAnimal = listAllytemp[i].posisi;
                    if (j == 0) tujuanAnimal += gerakAI(0, i, posisiAnimal);
                    else if (j == 1) tujuanAnimal += gerakAI(1, i, posisiAnimal);
                    else if (j == 2) tujuanAnimal += gerakAI(2, i, posisiAnimal); 
                    else if (j == 3) tujuanAnimal += gerakAI(3, i, posisiAnimal);

                    if (tujuanAnimal > 0 && tujuanAnimal < 63)
                    {
                        // if bawah harus di if (listp1 itu player p1) dan (listp2 itu player p2) ---------------------------------------------------------------------------------------
                        if (listAllytemp[i].status == "alive" && listAllytemp[i].possibleMove(tujuanAnimal, listMapAItemp, listAllytemp, listOpponenttemp, i))
                        {
                            //GERAK ANIMAL
                            listAllytemp[i].moveAnimal(listMapAItemp, listOpponenttemp, tujuanAnimal, i);

                            //RECURSIVE
                            score = -negaMax(depth - 1, Math.Max(alpha, max),beta*-1, player, listOpponenttemp, listAllytemp, listMapAItemp);
                            if (score > max)
                            {
                                //MessageBox.Show("score : " + score + "\nmax: " + max + "\ni: " + i + "\nj: "+ j + "\ndepth : " + depth);
                                max = score;
                            }
                            //if (max >= beta)
                            //{
                            //    return max;
                            //    break;
                            //}
                            //if (score > alpha)
                            //{
                            //    alpha = score;
                            //}
                        }
                    }
                }
            }
            return max;
        }
        private int evaluate(List<classMap> listMapAI, List<classAnimal> listAlly, List<classAnimal> listOpponent) {
            //string cetak = "";
            //for (int i = 0; i < 9; i++)
            //{
            //    for (int j = 0; j < 7; j++)
            //    {
            //        cetak += listMapAI[(i * 7) + j].indexAnimal;
            //    }
            //    cetak += "\n";
            //}
            //MessageBox.Show(cetak);
            
            int totalSBE = 0;
            totalSBE += SBEpower(listMapAI, listAlly, listOpponent);
            totalSBE += SBEdistanceGoal(listMapAI, listAlly, listOpponent);
            //totalSBE += SBEgotoWin(listMapAI, listAlly, listOpponent);
            //totalSBE += SBEgotoShield(listMapAI, listAlly, listOpponent);
            return totalSBE;
        }

        private int SBEpower(List<classMap> listMapAI, List<classAnimal> listAlly, List<classAnimal> listOpponent) {
            int total = 0;
            for (int i = 0; i < listAlly.Count; i++)
            {
                if (listAlly[i].status == "alive") total += listAlly[i].power;
            }
            for (int i = 0; i < listOpponent.Count; i++)
            {
                if (listOpponent[i].status == "alive") total -= listOpponent[i].power;
            }
            //if(listAlly[0].status == "dead")MessageBox.Show("Test" + total);
            //if (listAlly[0].player == LAWAN_AI) total *= -1;
            return total*10;
        }
        private int SBEdistanceGoal(List<classMap> listMapAI, List<classAnimal> listAlly, List<classAnimal> listOpponent) {
            int total = 0;
            for (int i = 0; i < listAlly.Count; i++) {
                Boolean cek = true;
                int ctr = 0;
                int posisi = listAlly[i].posisi;
                int hasil = 0;
                while (cek) {
                    if (posisi > 6)
                    {
                        posisi -= 7;
                        ctr++;
                    }
                    else cek = false;
                }
                hasil = 3 - posisi; if (hasil < 0) hasil *= -1;
                posisi = 3;
                if (listAlly[i].player == "p1")
                {
                    hasil += ctr;
                }
                else hasil += (8 - ctr);

                total += hasil;
                //MessageBox.Show(hasil + "");
            }
            //MessageBox.Show(total+"total");
            total = 80 - total;
            //if (listAlly[0].player == LAWAN_AI) total *= -1;
            return total*2;
        }
        private int SBEgotoWin(List<classMap> listMapAI, List<classAnimal> listAlly, List<classAnimal> listOpponent) {
            int hasil = 0;
            for (int i = 0; i < listAlly.Count; i++)
            {
                if (listAlly[i].posisi == 3 && listAlly[i].status == "alive")
                {
                    if (listAlly[i].player == "p1") hasil = 20000;
                }
                else if (listAlly[i].posisi == 59 && listAlly[i].status == "alive")
                {
                    if (listAlly[i].player == "p2") hasil = 20000;
                }
            }

            //if (listAlly[0].player == "p1") hasil *= -1;
            return hasil;
        }
        private int SBEgotoShield(List<classMap> listMapAI, List<classAnimal> listAlly, List<classAnimal> listOpponent) {
            int hasil = 0;
            for (int i = 0; i < listAlly.Count; i++)
            {
                int posisi = listAlly[i].posisi;
                if ((posisi == 52 || posisi == 58 || posisi == 60) && listAlly[i].player == "p2") {
                    //MessageBox.Show(posisi + "posisi");
                    if ((posisi + 7) < 63)
                    {
                        if (listMapAI[posisi + 1].indexAnimal == -1 && listMapAI[posisi - 1].indexAnimal == -1 && listMapAI[posisi + 7].indexAnimal == -1 && listMapAI[posisi - 7].indexAnimal == -1)
                        {
                            hasil = 10000;
                        }
                    }
                    else {
                        if (listMapAI[posisi + 1].indexAnimal == -1 && listMapAI[posisi - 1].indexAnimal == -1 && listMapAI[posisi - 7].indexAnimal == -1)
                        {
                            hasil = 10000;
                        }
                    }
                }
                else if ((posisi == 2 || posisi == 4 || posisi == 10) && listAlly[i].player == "p1")
                {
                    if ((posisi - 7) > 0)
                    {
                        if (listMapAI[posisi + 1].indexAnimal == -1 && listMapAI[posisi - 1].indexAnimal == -1 && listMapAI[posisi + 7].indexAnimal == -1 && listMapAI[posisi - 7].indexAnimal == -1)
                        {
                            hasil = 10000;
                        }
                    }
                    else
                    {
                        if (listMapAI[posisi + 1].indexAnimal == -1 && listMapAI[posisi - 1].indexAnimal == -1 && listMapAI[posisi + 7].indexAnimal == -1)
                        {
                            hasil = 10000;
                        }
                    }
                }
            }
            //if (listAlly[0].player == "p1") hasil *= -1;
            return hasil;
        }

        public void cloningGerakAwal() {
            //cloning data
            listMapAI = listMap.ConvertAll(classMap => classMap.Clone());
            if (turn == "p1")
            {
                listAnimalAI = listP1.ConvertAll(classAnimal => classAnimal.Clone());
                listAnimalAIOpp = listP2.ConvertAll(classAnimal => classAnimal.Clone());
            }
            else if (turn == "p2")
            {
                listAnimalAI = listP2.ConvertAll(classAnimal => classAnimal.Clone());
                listAnimalAIOpp = listP1.ConvertAll(classAnimal => classAnimal.Clone());
            }
        }
        public int gerakAI(int gerak, int animal, int posisiAnimal) { //buat gerak ai jika ada yang lompat
            //dipake pada saat ai gerak satu persatu pada recursive
            if (gerak == 0)
            {
                if (animal == 5 || animal == 6) {
                    if (posisiAnimal == 15 || posisiAnimal == 16 || posisiAnimal == 18 || posisiAnimal == 19) return 28;
                    else return 7;
                }
                else return 7;
            }
            else if (gerak == 1)
            {
                if (animal == 5 || animal == 6) 
                {
                    if (posisiAnimal == 43 || posisiAnimal == 44 || posisiAnimal == 46 || posisiAnimal == 47) return -28;
                    else return -7;
                }
                else return -7;
            }

            else if (gerak == 2)
            {
                if (animal == 5 || animal == 6)
                {
                    if (posisiAnimal == 21 || posisiAnimal == 28 || posisiAnimal == 35 || posisiAnimal == 24 || posisiAnimal == 31 || posisiAnimal == 38) return 3;
                    else return 1;
                }
                else return 1;
            }

            else if (gerak == 3)
            {
                if (animal == 5 || animal == 6)
                {
                    if (posisiAnimal == 27 || posisiAnimal == 34 || posisiAnimal == 41 || posisiAnimal == 24 || posisiAnimal == 31 || posisiAnimal == 38) return -3;
                    else return -1;
                }
                else return -1;
            }


            return -1;
        }


    }
}
